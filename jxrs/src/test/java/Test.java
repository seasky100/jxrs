import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class Test {
	private static Connection getConn() {
        String driver = "oracle.jdbc.driver.OracleDriver";
        String url = "jdbc:oracle:thin:@//10.150.152.100:1521/pdborcl";
        String username = "jxreport";// 用户名
        String password = "Jx123456";// 密码
        Connection conn = null; // 创建数据库连接对象
        try {
            Class.forName(driver);
            conn = DriverManager.getConnection(url, username, password);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return conn;
    }

    private static void query() {
        Connection conn = getConn();
        String sql = "SELECT 'x' FROM DUAL";
        PreparedStatement pstmt;
        try {
            pstmt = conn.prepareStatement(sql);
            // 建立一个结果集,用来保存查询出来的结果

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                String username = rs.getString("'X'");
                System.out.println(username);
            }
            rs.close();
            pstmt.close();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    
    public static void main(String[] args) {
        
        query();
    }
}
