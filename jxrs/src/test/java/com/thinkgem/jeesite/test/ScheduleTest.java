package com.thinkgem.jeesite.test;

import java.util.Date;

import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.thinkgem.jeesite.common.utils.DateUtils;

@Service
@Lazy(false)
public class ScheduleTest {
	
	@Scheduled(cron="0 0/1 * * * ?")//1分钟任务
	public void  testTask(){
		System.out.println(DateUtils.formatDateTime(new Date()));
	}
}
